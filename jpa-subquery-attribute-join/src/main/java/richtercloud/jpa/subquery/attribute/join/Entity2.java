package richtercloud.jpa.subquery.attribute.join;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author richter
 */
@Entity
public class Entity2 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    @ManyToOne
    private Entity3 entity3;

    public Entity2() {
    }

    public Entity2(Long id, Entity3 entity2) {
        this.id = id;
        this.entity3 = entity2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entity3 getEntity3() {
        return entity3;
    }

    public void setEntity3(Entity3 entity3) {
        this.entity3 = entity3;
    }
}
