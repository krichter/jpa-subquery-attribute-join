package richtercloud.jpa.subquery.attribute.join;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

/**
 *
 * @author richter
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Entity3 entity3 = new Entity3(1l, "some property value");
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("richtercloud_jpa-subquery-attribute-join_jar_1.0-SNAPSHOTPU" //persistenceUnitName
        );
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Entity0> offerRoot = cq.from(Entity0.class);
        Join<Entity0, Entity1> entity0Join = offerRoot.join(Entity0_.entity1,
                JoinType.LEFT);
        Root<Entity2> entity2JoinRoot = cq.from(Entity2.class);
            //can only specify a Class here while selecting from a join is
            //possible outside criteria api as well
        
        Join<Entity2, Entity3> entity2Join = cq.join(Entity1_.entity2s)
                .get(Entity1_.entity2s)
                .join(Entity2_.entity3);
        
        
        
        Subquery<Entity2> entity2Subquery = cq.subquery(Entity2.class);
        Root<Entity2> entity2SubqueryRoot = cq.from(Entity2.class);
        entity2Subquery.select(entity2SubqueryRoot)
                .where(cb.equal(entity2Join.get(Entity2_.entity3),
                        forUser));
        
        
        cq.select(cb.count(offerRoot))
                .where(cb.and(cb.isNotNull(offerRoot.get(Entity0_.pendingEdit)),
                        cb.not(cb.exists(subquery));
        
        
        
        
        Subquery<Entity2> reviewResultSubquery = cq.subquery(Entity2.class);
        Root<Entity2> reviewResultRoot = reviewResultSubquery.from(Entity2.class);
        reviewResultSubquery.where(cb.equal(reviewResultRoot.get(ReviewResult_.byUser), forUser));
        cq.where(cb.and(cb.notEqual(offerRoot.get(Entity0_.pendingEdit), cb.nullLiteral(Entity1.class))),
                cb.not(cb.exists(reviewResultSubquery)));
        TypedQuery<Long> offerQuery = entityManager.createQuery(cq);
        //try to figure this out without criteria API (more complex than
        //retrieving offer creations which need review like in
        //getOfferCreationReviewCount)
        TypedQuery<Long> query = entityManager.createQuery(
                "SELECT COUNT(e0) FROM Entity0 e0 LEFT JOIN e0.entity1 e1 "
                        + "WHERE e0.entity1 != null "
                        + "AND NOT EXISTS (SELECT e2 FROM e1.entity2s e2 LEFT OUTER JOIN e2.entity3 e3 WHERE e3.id = :someId)",
                Long.class)
                .setParameter("someId", entity3.getId());
            //join is necessary in order to be able to access
            //e0.entity1.entity2s because JPA isn't capable of
            //accessing a reference of a reference
        long retValue = query.getSingleResult();
    }
}
