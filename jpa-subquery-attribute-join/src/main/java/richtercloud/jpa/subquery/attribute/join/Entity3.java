package richtercloud.jpa.subquery.attribute.join;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author richter
 */
@Entity
public class Entity3 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    @Basic
    private String property0;

    public Entity3() {
    }

    public Entity3(Long id, String property0) {
        this.id = id;
        this.property0 = property0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProperty0() {
        return property0;
    }

    public void setProperty0(String property0) {
        this.property0 = property0;
    }
}
