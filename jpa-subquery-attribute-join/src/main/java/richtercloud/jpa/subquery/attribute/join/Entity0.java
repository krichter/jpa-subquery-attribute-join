package richtercloud.jpa.subquery.attribute.join;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author richter
 */
@Entity
public class Entity0 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    @OneToOne
    private Entity1 entity1;

    public Entity0() {
    }

    public Entity0(Long id, Entity1 entity1s) {
        this.id = id;
        this.entity1 = entity1s;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entity1 getEntity1() {
        return entity1;
    }

    public void setEntity1(Entity1 entity1) {
        this.entity1 = entity1;
    }
}
