package richtercloud.jpa.subquery.attribute.join;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author richter
 */
@Entity
public class Entity1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    @OneToMany
    private List<Entity2> entity2s;

    public Entity1() {
    }

    public Entity1(Long id, List<Entity2> entity1s) {
        this.id = id;
        this.entity2s = entity1s;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Entity2> getEntity2s() {
        return entity2s;
    }

    public void setEntity2s(List<Entity2> entity2s) {
        this.entity2s = entity2s;
    }
}
